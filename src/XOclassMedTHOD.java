import java.util.Scanner;
public class XOclassMedTHOD {
    public static char[] board = {' ',' ',' ',' ',' ',' ',' ',' ',' ',};
    public static int board_sp = 0;
    public static void setUp() {
        System.out.println("โปรดใส่ตัวเลขระหว่าง 1-9 \n");
        System.out.println(" 1 | 2 | 3 ");
        System.out.println("-----------");
        System.out.println(" 4 | 5 | 6 ");
        System.out.println("-----------");
        System.out.println(" 7 | 8 | 9 ");
        System.out.println("\n --- เริ่มเกม ---\n");
    }

    static void Dispaly() {
        System.out.println(" " + board[0] + " | " + board[1] + " | " + board[2]);
        System.out.println("-----------");
        System.out.println(" " + board[3] + " | " + board[4] + " | " + board[5]);
        System.out.println("-----------");
        System.out.println(" " + board[6] + " | " + board[7] + " | " + board[8]);
    }

    static boolean check(int x) {
        int[] board_range = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int i = 0; i < 9; i++) {
            if (x == board_range[i]) {
                return true;
            }
        }
        return false;
    }

    static void Userin(boolean x_s) {
        Scanner sc = new Scanner(System.in);
        if (x_s) {
            System.out.println("X turn input number: ");
        } else {
            System.out.println("O turn input number: ");
        }
        try {
            String Inputty = sc.next();
            int u = Integer.parseInt(Inputty);
            if (check(u)) {
                if (board[u - 1] == ' ') {
                    if (x_s) {
                        board[u - 1] = 'X';
                    } else {
                        board[u - 1] = 'O';
                    }
                    board_sp += 1;
                } else {
                    System.out.println("อย่าเอ๋อ\n");
                }
            } else {
                System.out.println("ใส่ 1 - 9 น้องชาย");
            }
        } catch (Exception e) {
            System.out.println("ใส่เลขไอ่สัส");
        }
    }

    static boolean[] checkWin() {
        boolean[] ans = new boolean[2];
        //ชนะแนวนอน
        if (board[0] == board[1] && board[0] == board[2] && board[0] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        if (board[3] == board[4] && board[3] == board[5] && board[3] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        if (board[6] == board[7] && board[6] == board[8] && board[6] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        //ตั้ง
        if (board[0] == board[3] && board[0] == board[6] && board[0] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        if (board[1] == board[4] && board[1] == board[7] && board[1] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        if (board[2] == board[5] && board[2] == board[8] && board[2] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        //ทแยง
        if (board[0] == board[4] && board[0] == board[8] && board[0] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        if (board[2] == board[4] && board[2] == board[6] && board[2] != ' ') {
            ans[0] = false;
            ans[1] = false;
            return ans;
        }
        if (board_sp == 9) {
            ans[0] = false;
            ans[1] = true;
            return ans;
        }
        ans[0] = true;
        ans[1] = false;
        return ans;
    }
}

